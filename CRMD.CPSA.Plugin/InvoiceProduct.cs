﻿namespace CRMD.CPSA.Plugin
{
    using CRMD.CPSA.Plugin.Helper;
    using Microsoft.Xrm.Sdk;
    using System;

    public class InvoiceProduct : PluginBase, IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            string step = string.Empty;

            try
            {
                base.Initialize(serviceProvider);

                if (this.Context.Depth > 1) return;

                var invoice = this.TargetEntity;

                Guid invoiceId = ((EntityReference)invoice.Attributes["invoiceid"]).Id;

                InvoiceHelper.UpdateParentInvoice(this.Service, invoiceId);
            }
            catch (Exception ex)
            {
                this.Tracing.Trace(string.Format("Error:{0}. The error occured at {1}.", ex.Message, step));
                throw new InvalidPluginExecutionException("An error occured in Invoice Post Create Plugin, please check tracing logs for more info.");
            }
        }
    }
}