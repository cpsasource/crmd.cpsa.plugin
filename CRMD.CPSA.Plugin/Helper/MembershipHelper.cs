﻿using CRMD.CPSA.Plugin.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace CRMD.CPSA.Plugin.Helper
{
    public class MembershipHelper
    {
        static DateTime minCrmDate = new DateTime(1901, 1, 1);

        public static Timeline GetTimeline(Entity membershipEntity, DateTime initDate)
        {
            return GetTimeline(membershipEntity, initDate, true);
        }

        public static Timeline GetTimeline(Entity membershipEntity, DateTime initDate, bool increaseMonth)
        {
            DateTime FirstDayofMonth = new DateTime(initDate.Year, initDate.Month, 1);
            DateTime startDate = FirstDayofMonth;
            int targetmonth = 12;
            DateTime endDate = DateTime.MinValue;

            if (membershipEntity != null && membershipEntity.Attributes.Contains("adx_membershipterm"))
            {
                OptionSetValue term = membershipEntity.GetAttributeValue<OptionSetValue>("adx_membershipterm");

                if (term.Value == 3 || term.Value == 790410000)//Annual or BiAnnual
                {
                    if (term.Value == 3)//Annual
                    {
                        targetmonth = 12;
                    }
                    else if (term.Value == 790410000)//BiAnnual
                    {
                        targetmonth = 24;
                    }
                }
            }
            var TempLastDate = startDate.AddMonths(targetmonth);

            //this code is from Mona "MembershipUpdate.cs" in another plugin
            if (increaseMonth)
            {
                if (TempLastDate.Month != 12)
                    endDate = new DateTime(TempLastDate.Year, TempLastDate.Month + 1, 1);
                if (TempLastDate.Month == 12)
                    endDate = new DateTime(TempLastDate.Year + 1, 1, 1);
            }
            else
            {
                endDate = new DateTime(TempLastDate.Year, TempLastDate.Month, 1);
            }

            endDate = endDate.AddDays(-1);
            return new Timeline { StartDate = startDate, EndDate = endDate };
        }

        public static void CheckReferralFromMembership(IOrganizationService service, Entity entityPreImage, Entity entity, ITracingService tracingService, DateTime paymentDate)
        {
            //Referral
            try
            {
                if (entityPreImage.Attributes.Contains("crmd_referrallkup"))
                {
                    Entity membershipRef = service.Retrieve("crmd_membershipinvitation", entityPreImage.GetAttributeValue<EntityReference>("crmd_referrallkup").Id, new ColumnSet(true));

                    if (membershipRef != null)
                    {

                        DateTime referredOn = membershipRef.GetAttributeValue<DateTime?>("crmd_referredon") ?? DateTime.MinValue;
                        if (paymentDate.Year != referredOn.Year)
                        {
                            //Referral expired 790410000
                            if (membershipRef.GetAttributeValue<OptionSetValue>("statecode").Value == 0)
                                EntityHelper.SetEntityState(service, membershipRef.LogicalName, membershipRef.Id, false, 790410000);
                        }
                        if (paymentDate.Year == referredOn.Year)
                        {
                            //Payment Year == Referred On Year",  happen in the same year
                            EntityReference referredBy = membershipRef.GetAttributeValue<EntityReference>("crmd_referredbyid");

                            if (referredBy == null || referredBy.Id == Guid.Empty)
                                return;

                            //if prize already exist just increment with 1 the referral count
                            Entity contactPriveLevel = ContactPrizeLevelHelper.GetByContactAndPeriod(service, referredBy.Id, referredOn);
                            if (contactPriveLevel != null)
                            {
                                int currentPrizeCount = contactPriveLevel.GetAttributeValue<int>("crmd_currentcount");
                                currentPrizeCount++;

                                //one PrizeLevelSetup record has field "altus_PrizeLevel" value is 0, so all integer should come back with something
                                var prizeLevel = PrizeLevelSetupHelper.GetPrizeLevelSetupByLevel(service, currentPrizeCount);

                                //if this membershipInvitation do not have completed membership value
                                if (prizeLevel != null && !membershipRef.Attributes.Contains("crmd_completedmembershipid"))
                                {
                                    membershipRef["crmd_completedmembershipid"] = new EntityReference(entity.LogicalName, entity.Id);

                                    service.Update(membershipRef);
                                    if (membershipRef.GetAttributeValue<OptionSetValue>("statecode").Value == 0)
                                        EntityHelper.SetEntityState(service, membershipRef.LogicalName, membershipRef.Id, false, 2);
                                }
                            }
                            //if not, create the prize with default values
                            else
                            {
                                Entity period = PeriodLookupHelper.GetPeriodLookupByCurrentDate(service, referredOn);

                                //ColumnSet attributes = new ColumnSet() { AllColumns = true };
                                Entity contactR = service.Retrieve("contact", referredBy.Id, new ColumnSet(new[] { "fullname" }));

                                //this contact is actually the referral, not referredBy on
                                //var contact = Contact.GetContact(service, ((EntityReference)entityPreImage.Attributes["altus_contactid"]).Id);
                                if (contactR != null)
                                {
                                    string periodName = string.Empty;
                                    string contactFullName = string.Empty;
                                    if (period.Attributes.Contains("altus_name"))
                                        periodName = period["altus_name"].ToString();

                                    if (contactR.Attributes.Contains("fullname"))
                                        contactFullName = contactR["fullname"].ToString();
                                    string prizeName = string.Empty;
                                    prizeName = periodName + '-' + contactFullName;
                                    ContactPrizeLevelHelper.CreateContactPrizeLevel(service, prizeName, referredBy.Id, period.Id, PrizeLevelSetupHelper.GetPrizeLevelSetupByLevel(service, 0).Id);

                                }
                                if (membershipRef.GetAttributeValue<OptionSetValue>("statecode").Value == 0)
                                    EntityHelper.SetEntityState(service, membershipRef.LogicalName, membershipRef.Id, false, 2);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void UpdateContactInfoFromMembership(IOrganizationService service, Entity entityPreImage, Entity entity, DateTime startDate, DateTime endDate, string MessageName)
        {
            //this section cause TFS1010
            if (entityPreImage.Attributes.Contains("crmd_contactid"))
            {
                Entity contact = service.Retrieve("contact", entityPreImage.GetAttributeValue<EntityReference>("crmd_contactid").Id, new ColumnSet(true));
                Guid priceId = Guid.Empty;
                if (entityPreImage.Attributes.Contains("crmd_membershiptypeid"))
                {
                    Entity membershipType = service.Retrieve("crmd_membershiptype", entityPreImage.GetAttributeValue<EntityReference>("crmd_membershiptypeid").Id, new ColumnSet(true));

                    if (membershipType.Attributes.Contains("crmd_membershiptypepricelist"))
                    {
                        priceId = ((EntityReference)membershipType.Attributes["crmd_membershiptypepricelist"]).Id;
                    }
                }

                if (contact != null)
                {
                    Entity contactToUpdate = new Entity("contact");
                    contactToUpdate.Id = contact.Id;

                    bool doUpdate = false;
                    if (contact.GetAttributeValue<OptionSetValue>("crmd_membershiptype") != entityPreImage.GetAttributeValue<OptionSetValue>("crmd_membershiptype"))
                    {
                        contactToUpdate["crmd_membershiptype"] = entityPreImage["crmd_membershiptype"];
                        doUpdate = true;
                    }

                    if (contact.GetAttributeValue<OptionSetValue>("crmd_membershipcat") != entityPreImage.GetAttributeValue<OptionSetValue>("crmd_membershipcategory"))
                    {
                        contactToUpdate["crmd_membershipcat"] = entityPreImage["crmd_membershipcategory"];
                        doUpdate = true;
                    }

                    //set contact to be current
                    if (contact.GetAttributeValue<OptionSetValue>("crmd_membershipstatus").Value != 790410000)
                    {
                        contactToUpdate["crmd_membershipstatus"] = new OptionSetValue(790410000);
                        doUpdate = true;
                    }

                    if (endDate > minCrmDate && endDate > contact.GetAttributeValue<DateTime>("crmd_nextrenewaldatemembership"))
                    {
                        contactToUpdate["crmd_nextrenewaldatemembership"] = endDate;
                        doUpdate = true;
                    }

                    if (entityPreImage.GetAttributeValue<bool>("crmd_reinstatement"))
                    {
                        var paidDate = GetLatestDateTime(entityPreImage, entity, "crmd_paymentdate");
                        if (paidDate > minCrmDate && paidDate != contact.GetAttributeValue<DateTime>("crmd_datemembershipreinstated"))
                        {
                            contactToUpdate["crmd_datemembershipreinstated"] = paidDate;
                            doUpdate = true;
                        }
                    }

                    if (startDate > minCrmDate && !contact.Attributes.Contains("crmd_membershipdatefirstjoined"))
                    {
                        contactToUpdate["crmd_membershipdatefirstjoined"] = startDate;
                        doUpdate = true;
                    }

                    if (endDate > minCrmDate && endDate > contact.GetAttributeValue<DateTime>("crmd_membershippaidthru"))
                    {
                        doUpdate = true;
                        contactToUpdate["crmd_membershippaidthru"] = endDate;
                    }

                    //if user did not have express consent, then change impled consent
                    if (endDate > minCrmDate && !contact.Attributes.Contains("crmd_expressdate"))
                    {
                        DateTime end2years = endDate.AddYears(2);
                        if (!contact.Attributes.Contains("crmd_implieddate") || end2years > contact.GetAttributeValue<DateTime>("crmd_implieddate"))
                        {
                            doUpdate = true;
                            contactToUpdate["altus_implieddate"] = end2years;
                        }
                    }

                    if (!priceId.Equals(Guid.Empty))
                    {
                        doUpdate = true;
                        //the Price List in the associated Current membership setup rule is used for the Price List of Contact
                        contactToUpdate["defaultpricelevelid"] = new EntityReference("pricelevel", priceId);
                    }

                    if (doUpdate)
                    {
                        service.Update(contactToUpdate);
                    }
                }
            }
        }

        public static DateTime GetLatestDateTime(Entity preImage, Entity entity, string fieldName)
        {
            DateTime retDate = DateTime.MinValue;
            if (preImage.Attributes.Contains(fieldName))
                retDate = preImage.GetAttributeValue<DateTime>(fieldName);
            if (entity.Attributes.Contains(fieldName))
                retDate = entity.GetAttributeValue<DateTime>(fieldName);

            return retDate;
        }
    }
}
