﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Linq;

namespace CRMD.CPSA.Plugin.Helper
{
    public class MembershipReferralHelper
    {
        public static Entity GetMembershipReferral(IOrganizationService service, Guid membershipGuid)
        {
            var fetchXml =
            string.Format(@"<fetch mapping='logical' version='1.0'>
	                <entity name='crmd_membershipinvitation'>
                    <attribute name='crmd_membershipinvitationid' />
                    <attribute name='crmd_name' />
                    <attribute name='createdon' />
                    <attribute name='crmd_referredon' />
                    <attribute name='crmd_referredbyid' />
                    <order attribute='crmd_name' descending='false' />
                    <filter type='and'>
                      <condition attribute='crmd_membershipinvitationid' operator='eq' value='{0}' />
                    </filter>
                  </entity>
                </fetch>", membershipGuid);

            // Run the query with the FetchXML.
            return service.RetrieveMultiple(new FetchExpression(fetchXml)).Entities.FirstOrDefault();
        }
    }
}
