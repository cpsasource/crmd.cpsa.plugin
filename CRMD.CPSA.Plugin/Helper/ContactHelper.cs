﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Linq;

namespace CRMD.CPSA.Plugin.Helper
{
    public class ContactHelper
    {
        public static Entity Get(IOrganizationService service, Guid id, int statecode = 0)
        {
            return service.RetrieveMultiple(new QueryExpression
            {
                EntityName = "contact",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Conditions =
                    {
                        new ConditionExpression("contactid", ConditionOperator.Equal, id),
                        new ConditionExpression("statecode", ConditionOperator.Equal, statecode),
                    }
                }
            }).Entities.FirstOrDefault();
        }
    }
}
