﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Linq;

namespace CRMD.CPSA.Plugin.Helper
{
    public class PrizeLevelSetupHelper
    {
        public static Entity GetPrizeLevelSetupByLevel(IOrganizationService service, int level)
        {
            // This statement is required to enable early-bound type support.
            QueryExpression mySavedQuery = new QueryExpression
            {
                EntityName = "crmd_prizelevelsetup",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    FilterOperator = LogicalOperator.And,
                    Filters =
                        {
                            new FilterExpression
                            {
                                FilterOperator = LogicalOperator.And,
                                Conditions =
                                {
                                    new ConditionExpression("crmd_prizelevel", ConditionOperator.LessEqual, level)
                                }
                            },
                        }
                },
                Orders = {
                    new OrderExpression{OrderType = OrderType.Descending,AttributeName="crmd_prizelevel"}
                }
            };
            
            return service.RetrieveMultiple(mySavedQuery).Entities.FirstOrDefault();
        }
    }
}
