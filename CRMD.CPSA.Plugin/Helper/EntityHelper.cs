﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;
using System.Linq;

namespace CRMD.CPSA.Plugin.Helper
{
    public class EntityHelper
    {
        public static string SplitAliasedAttributeEntityName(ref string attributeName)
        {
            string aliasedEntityName = null;
            if (attributeName.Contains('.'))
            {
                var split = attributeName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length != 2)
                {
                    throw new Exception("Attribute Name was specified for an Alaised Value with " + split.Length +
                    " split parts, and two were expected.  Attribute Name = " + attributeName);
                }
                aliasedEntityName = split[0];
                attributeName = split[1];
            }

            return aliasedEntityName;
        }

        public static void SetEntityState(IOrganizationService service, string entityName, Guid entityId, bool isActive, int status)
        {
            service.Execute(new SetStateRequest
            {
                EntityMoniker = new EntityReference
                {
                    Id = entityId,
                    Name = entityName,
                    LogicalName = entityName
                },
                State = new OptionSetValue(isActive ? 0 : 1),
                Status = new OptionSetValue(status)
            });
        }
    }
}
