﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Linq;

namespace CRMD.CPSA.Plugin.Helper
{
    public class PeriodLookupHelper
    {
        public static Entity GetPeriodLookupByCurrentDate(IOrganizationService service, DateTime referredOn)
        {
            var fetchXml =
            string.Format(@"<fetch mapping='logical' version='1.0'>
	                <entity name='altus_periodlookup'>
		               <attribute name='altus_name' />
                        <attribute name='altus_periodlookupid' />
                        <order attribute='altus_name' descending='false' />
                        <filter type='and'>
                            <condition attribute='altus_startdate' operator='on-or-before' value='{0}' />
                            <condition attribute='altus_enddate' operator='on-or-after' value='{0}' />
                        </filter>
	                </entity>
                </fetch>", referredOn.Date);

            // Run the query with the FetchXML.
            var fetchExpression = new FetchExpression(fetchXml);
            return service.RetrieveMultiple(fetchExpression).Entities.FirstOrDefault();
        }
    }
}
