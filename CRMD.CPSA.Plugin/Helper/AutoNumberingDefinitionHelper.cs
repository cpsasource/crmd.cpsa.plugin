﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Linq;

namespace CRMD.CPSA.Plugin.Helper
{
    public class AutoNumberingDefinitionHelper
    {
        static string EntityName = "adx_autonumberingdefinition";

        public static Entity GetByName(IOrganizationService service, string name, params string[] columns)
        {
            return service.RetrieveMultiple(new QueryExpression
            {
                EntityName = EntityName,
                ColumnSet = columns != null && columns.Any() ? new ColumnSet(columns) : new ColumnSet(true),
                Criteria =
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode", ConditionOperator.Equal, 0),
                        new ConditionExpression("adx_name", ConditionOperator.Equal, name),
                    }
                }
            }).Entities.FirstOrDefault();
        }
    }
}
