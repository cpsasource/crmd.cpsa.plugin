﻿using CRMD.CPSA.Plugin.Extension;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Linq;

namespace CRMD.CPSA.Plugin.Helper
{
    public class InvoiceHelper
    {
        public static Entity GetTotalTaxLevelsByInvoice(IOrganizationService service, Guid invoiceId)
        {
            var fetchXml =
            string.Format(@"<fetch mapping='logical' version='1.0' aggregate='true'>
                <entity name='invoicedetail' >
                    <attribute name='altus_taxlevel1' aggregate='sum' alias='altus_taxlevel1' />
                    <attribute name='altus_taxlevel2' aggregate='sum' alias='altus_taxlevel2'/>
                    <attribute name='altus_taxlevel3' aggregate='sum' alias='altus_taxlevel3'/>
                    <link-entity name='invoice' from='invoiceid' to='invoiceid' alias='aa'>
                      <filter type='and'>
                        <condition attribute='invoiceid' operator='eq' value='{0}' />
                      </filter>
                    </link-entity>
                </entity>
            </fetch>", invoiceId);


            var fetchExpression = new FetchExpression(fetchXml);
            return service.RetrieveMultiple(fetchExpression).Entities.FirstOrDefault();
        }

        public static void UpdateParentInvoice(IOrganizationService service, Guid parentInvoiceId)
        {
            var taxLevels = GetTotalTaxLevelsByInvoice(service, parentInvoiceId);
            if (taxLevels != null)
            {
                bool goUpdate = false;

                string[] parentFieldsArray = new string[] { "crmd_totaltaxlevel1", "crmd_totaltaxlevel2", "crmd_totaltaxlevel3" };

                Entity parentInvoice = service.Retrieve("invoice", parentInvoiceId, new ColumnSet(parentFieldsArray));
                string[] taxLevelsArray = new string[] { "crmd_taxlevel1", "crmd_taxlevel2", "crmd_taxlevel3" };
                if (parentInvoice != null)
                {
                    for (var i = 0; i < taxLevelsArray.Length; i++)
                    {
                        if (taxLevels.Attributes.Contains(taxLevelsArray[i]))
                        {
                            var tempAV = taxLevels.GetAliasedValue<Money>(taxLevelsArray[i]);
                            if (tempAV != null && tempAV.Value >= 0)
                            {
                                if (!parentInvoice.Attributes.Contains(parentFieldsArray[i]) || parentInvoice.GetAttributeValue<Money>(parentFieldsArray[i]).Value != tempAV.Value)
                                {
                                    goUpdate = true;
                                    parentInvoice[parentFieldsArray[i]] = tempAV;
                                }
                            }
                            else if (tempAV == null)
                            {
                                if (parentInvoice.Attributes.Contains(parentFieldsArray[i]) && parentInvoice.GetAttributeValue<Money>(parentFieldsArray[i]).Value > 0)
                                {
                                    goUpdate = true;
                                    parentInvoice[parentFieldsArray[i]] = new Money(0m);
                                }
                            }
                        }
                    }

                    //Get total of invoice and compare to order total
                    Entity currentInvoice = service.Retrieve("invoice", parentInvoiceId, new ColumnSet(new string[] { "salesorderid", "totalamount" }));
                    Entity currentOrder = service.Retrieve("salesorder", currentInvoice.GetAttributeValue<EntityReference>("salesorderid").Id, new ColumnSet(new string[] { "totalamount" }));

                    decimal invoiceTotal = currentInvoice.GetAttributeValue<Money>("totalamount").Value;
                    decimal orderTotal = currentOrder.GetAttributeValue<Money>("totalamount").Value;

                    decimal marginOfError = invoiceTotal - orderTotal;

                    if (goUpdate)
                    {
                        service.Update(parentInvoice);
                    }
                }
            }
        }
    }
}
