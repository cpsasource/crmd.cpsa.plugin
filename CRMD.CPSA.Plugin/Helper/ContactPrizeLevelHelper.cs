﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Linq;

namespace CRMD.CPSA.Plugin.Helper
{
    public class ContactPrizeLevelHelper
    {
        public static Entity GetByContactAndPeriod(IOrganizationService service, Guid contactId, DateTime date)
        {
            var fetchXml =
            string.Format(@"<fetch mapping='logical' version='1.0'>
	                <entity name='crmd_contactprizelevel'>
		               <attribute name='name_name' />
                        <attribute name='createdon' />
                        <attribute name='crmd_prizelevelachieved' />
                        <attribute name='crmd_currentcount' />
                        <attribute name='crmd_contactlkup' />
                        <attribute name='crmd_referralcount' />
                        <attribute name='modifiedon' />
                        <attribute name='crmd_compmembership' />
                        <attribute name='crmd_programperiodlkup' />
                        <attribute name='crmd_contactprizelevelid' />
                        <order attribute='crmd_name' descending='false' />
		                <filter type='and'>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='crmd_contactlkup' operator='eq' value='{0}' />
                        </filter>
                        <link-entity name='crmd_periodlookup' from='crmd_periodlkupid' to='crmd_programperiodlkup' alias='ac'>
                          <filter type='and'>
                            <filter type='and'>
                              <condition attribute='crmd_startdate' operator='on-or-before' value='{1}' />
                              <condition attribute='crmd_enddate' operator='on-or-after' value='{2}' />
                            </filter>
                          </filter>
                        </link-entity>
	                </entity>
                </fetch>", contactId, date.Date);

            var fetchExpression = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchExpression);

            return results.Entities.FirstOrDefault();
        }

        public static Guid CreateContactPrizeLevel(IOrganizationService service, string name, Guid contactLookupGuId, Guid periodLookupGuid, Guid prizeLevelSetupGuid)
        {
            Entity cplNew = new Entity("altus_contactprizelevel");
            cplNew["altus_name"] = name;
            cplNew["altus_currentcount"] = 0;  //field "New Members" on "Contact Prize Level" form
            cplNew["altus_referralcount"] = 1;//Total Referrals
            cplNew["altus_compmembership"] = new OptionSetValue(790410000);

            if (contactLookupGuId != Guid.Empty)
                cplNew["altus_contactlkup"] = new EntityReference("contact", contactLookupGuId);
            cplNew["altus_programperiodlkup"] = new EntityReference("altus_periodlookup", periodLookupGuid);
            cplNew["altus_prizelevelachieved"] = new EntityReference("altus_prizelevelsetup", prizeLevelSetupGuid);

            return service.Create(cplNew);
        }
    }
}
