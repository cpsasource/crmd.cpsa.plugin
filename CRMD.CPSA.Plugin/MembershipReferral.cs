﻿namespace CRMD.CPSA.Plugin
{
    using CRMD.CPSA.Plugin.Helper;
    using Microsoft.Xrm.Sdk;
    using System;

    public class MembershipReferral : PluginBase, IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            try
            {
                base.Initialize(serviceProvider);

                var entity = this.TargetEntity;
                if (entity == null) return;
                
                //adx_membershipinvitation is Entity "Membership Referral"
                if (entity.LogicalName == "crmd_membershipinvitation")
                {
                    //verify if membership exist
                    Entity mr = MembershipReferralHelper.GetMembershipReferral(this.Service, entity.Id);
                    if (mr == null) return;

                    DateTime referredOn = mr.GetAttributeValue<DateTime?>("crmd_referredon") ?? DateTime.Now;

                    EntityReference referredBy = mr.GetAttributeValue<EntityReference>("crmd_referredbyid");
                    if (referredBy == null) return;

                    //if prize already exist just increment with 1 the referral count
                    Entity contactPrizeLevel = ContactPrizeLevelHelper.GetByContactAndPeriod(this.Service, referredBy.Id, referredOn);
                    if (contactPrizeLevel != null)
                    {
                        int referralCount = contactPrizeLevel.GetAttributeValue<int>("crmd_referralcount") + 1;
                        int currentCount = contactPrizeLevel.GetAttributeValue<int>("altus_currentcount") + 1;

                        Entity cplUpdate = new Entity("crmd_contactprizelevel");
                        cplUpdate.Id = contactPrizeLevel.Id;

                        cplUpdate["crmd_referralcount"] = referralCount;

                        cplUpdate.EntityState = EntityState.Changed;
                        this.Service.Update(cplUpdate);
                    }
                    //if not create the prize with default values
                    else
                    {
                        Entity period = PeriodLookupHelper.GetPeriodLookupByCurrentDate(this.Service, referredOn);
                        Entity contact = ContactHelper.Get(this.Service, referredBy.Id);
                        string periodName = period.GetAttributeValue<string>("crmd_name");
                        string contactFullName = contact.GetAttributeValue<string>("fullname");

                        string prizeName = periodName + '-' + contactFullName;
                        Guid newCPL = ContactPrizeLevelHelper.CreateContactPrizeLevel(this.Service, prizeName, referredBy.Id, period.Id, PrizeLevelSetupHelper.GetPrizeLevelSetupByLevel(this.Service, 0).Id);

                        Entity cplEntity = new Entity("crmd_contactprizelevel");
                        cplEntity.Id = newCPL;
                        cplEntity["crmd_referralcount"] = 1;

                        this.Service.Update(cplEntity);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}