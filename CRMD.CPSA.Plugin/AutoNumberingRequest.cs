﻿namespace CRMD.CPSA.Plugin
{
    using CRMD.CPSA.Plugin.Helper;
    using Microsoft.Xrm.Sdk;
    using System;
    using System.Linq;

    public class AutoNumberingRequest : PluginBase, IPlugin
    {
        private const string CONTACT_AUTO_NUMBERING = "Contact Auto-Numbering";
        private const string COMPANY_AUTO_NUMBERING = "Company Auto-Numbering";
        private const string PAYMENT_AUTO_NUMBERING = "Payment Auto-Numbering";
        
        string[] definitions = new string[] { CONTACT_AUTO_NUMBERING, COMPANY_AUTO_NUMBERING, PAYMENT_AUTO_NUMBERING };

        public void Execute(IServiceProvider serviceProvider)
        {
            string step = "";
            try
            {
                base.Initialize(serviceProvider);

                var entity = this.TargetEntity;
                if (entity == null) return;

                this.Tracing.Trace(string.Format("Check Message={0}, Depth={1}", this.Context.MessageName, this.Context.Depth));
                if (this.Context.MessageName.ToLower() != "create")
                    return;

                string definition = entity.GetAttributeValue<string>("adx_name");
                this.Tracing.Trace(string.Format("Check Auto Numbering Request, Definition={0}", definition));
                
                if (definitions.Contains(definition))
                {
                    step = "Retrieve Auto Numbering Definition.";
                    this.Tracing.Trace(step);
                    Entity autoNumberingDefinition = AutoNumberingDefinitionHelper.GetByName(this.Service, definition, "adx_name", "adx_lastnumberissued", "adx_format", "adx_digits");
                    if (autoNumberingDefinition != null)
                    {
                        int lastNumber = autoNumberingDefinition.GetAttributeValue<int>("adx_lastnumberissued");

                        int newNumber = lastNumber + 1;
                        string formattedNumber = string.Format(autoNumberingDefinition.GetAttributeValue<string>("adx_format"), newNumber);

                        step = string.Format("Update Auto Numbering Definition, LastNumber={0}.", newNumber);
                        this.Tracing.Trace(step);
                        Entity updateDefinitionEntity = new Entity(autoNumberingDefinition.LogicalName, autoNumberingDefinition.Id);
                        updateDefinitionEntity["adx_lastnumberissued"] = newNumber;
                        this.Service.Update(updateDefinitionEntity);

                        step = string.Format("Update Auto Numbering Request, Number={0}, FormattedNumber={1}.", newNumber, formattedNumber);
                        this.Tracing.Trace(step);
                        Entity updateRequestEntity = new Entity(entity.LogicalName, entity.Id);
                        updateRequestEntity["adx_number"] = newNumber;
                        updateRequestEntity["adx_formattednumber"] = formattedNumber;
                        this.Service.Update(updateRequestEntity);
                    }
                }

            }
            catch (Exception ex)
            {
                this.Tracing.Trace(string.Format("Error:{0}. The error occured at {1}.", ex.Message, step));
                throw new InvalidPluginExecutionException("An error occured in Auto Numbering Request Post Create Plugin, please check tracing logs for more info.");
            }
        }
    }
}