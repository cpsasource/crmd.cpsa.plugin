﻿namespace CRMD.CPSA.Plugin
{
    using CRMD.CPSA.Plugin.Helper;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using System;

    /// <summary>
    /// This plugin will be triggerred when deactivating Membership plugin
    /// </summary>
    public class MembershipReferralStatusChanged : PluginBase, IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            string step = string.Empty;

            try
            {
                base.Initialize(serviceProvider);

                if (this.Context.Depth > 1) return;

                EntityReference membershipReferralReference = this.EntityMoniker;
                step = "Initialization check target entity";

                if (membershipReferralReference != null && membershipReferralReference.LogicalName == "crmd_membershipinvitation")
                {
                    OptionSetValue state = (OptionSetValue)this.Context.InputParameters["State"];
                    OptionSetValue status = (OptionSetValue)this.Context.InputParameters["Status"];

                    //state is 1="Inactive" and Status is 2="Inactive"
                    if (state.Value == 1 && status.Value == 2)
                    {
                        Entity membershipReferral = this.Service.Retrieve(membershipReferralReference.LogicalName, this.Context.PrimaryEntityId, new ColumnSet(true));

                        DateTime referredOn = membershipReferral.GetAttributeValue<DateTime?>("crmd_referredon").GetValueOrDefault(DateTime.Now);

                        EntityReference referredBy = membershipReferral.GetAttributeValue<EntityReference>("crmd_referredbyid");
                        if (referredBy == null)
                        {
                            return;
                        }

                        Entity contactPrizeLevel = ContactPrizeLevelHelper.GetByContactAndPeriod(this.Service, referredBy.Id, referredOn);

                        if (contactPrizeLevel != null)
                        {
                            int referralCount = contactPrizeLevel.GetAttributeValue<int>("crmd_referralcount");
                            int currentCount = contactPrizeLevel.GetAttributeValue<int>("crmd_currentcount") + 1;

                            //cannot greater than referral count
                            if (currentCount > referralCount)
                            {
                                return;
                            }

                            Entity cplUpdate = new Entity("crmd_contactprizelevel");
                            cplUpdate.Id = contactPrizeLevel.Id;

                            //Get the prize level achieved based on the new referral count
                            Entity prizeLevelSetup = PrizeLevelSetupHelper.GetPrizeLevelSetupByLevel(this.Service, currentCount);

                            Entity contact = this.Service.Retrieve(referredBy.LogicalName, referredBy.Id, new ColumnSet(new string[] { "crmd_membershiptype" }));
                            int corporateReferrerType = 790410000;
                            bool isCorporateReferrer = contact != null && contact.GetAttributeValue<OptionSetValue>("crmd_membershiptype").Value == corporateReferrerType;

                            if (prizeLevelSetup != null && (!contactPrizeLevel.Attributes.Contains("crmd_prizelevelachieved") ||
                                contactPrizeLevel.GetAttributeValue<EntityReference>("crmd_prizelevelachieved").Id != prizeLevelSetup.Id))
                            {
                                //Prize level needs to be changed based on new referral count
                                cplUpdate["crmd_prizelevelachieved"] = new EntityReference("crmd_prizelevelsetup", prizeLevelSetup.Id);
                                cplUpdate["crmd_dateachieved"] = DateTime.Now;

                                if (prizeLevelSetup.GetAttributeValue<int>("crmd_prizelevel") == currentCount &&
                                    prizeLevelSetup.GetAttributeValue<Boolean>("crmd_complementarymembership") && !isCorporateReferrer)
                                {
                                    //The new or current prize level is eligible for a complementary membership
                                    cplUpdate["crmd_compmembership"] = new OptionSetValue(790410001);
                                }
                            }

                            cplUpdate["crmd_currentcount"] = currentCount;

                            cplUpdate.EntityState = EntityState.Changed;
                            this.Service.Update(cplUpdate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occured in Membership Referral Plugin, please check tracing logs for more info.");
            }
        }
    }
}