﻿using System;

namespace CRMD.CPSA.Plugin.Model
{
    public class Timeline
    {
        public Timeline()
        {
            StartDate = DateTime.MinValue;
            EndDate = DateTime.MinValue;
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
