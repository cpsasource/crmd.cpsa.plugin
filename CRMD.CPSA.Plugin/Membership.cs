﻿namespace CRMD.CPSA.Plugin
{
    using CRMD.CPSA.Plugin.Helper;
    using CRMD.CPSA.Plugin.Model;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using System;

    public class Membership : PluginBase, IPlugin
    {
        public static DateTime minCrmDate = new DateTime(1901, 1, 1);

        public void Execute(IServiceProvider serviceProvider)
        {
            try
            {
                base.Initialize(serviceProvider);

                var entity = this.TargetEntity;
                if (entity == null) return;

                //it could reach 7 depth
                //7 adx_name
                //6 altus_numberofactivememberships
                //5 adx_name
                //4 altus_membershipstatu change
                if (this.Context.MessageName.ToLower() == "update" && this.Context.Depth > 6)
                {
                    return;
                }

                var entityPreImage = entity;
                if (this.Context.MessageName.ToLower() == "update")
                {
                    entityPreImage = this.PreEntity;
                }

                string parentMemField = "crmd_parentcompanymembershipid";
                if (this.Context.MessageName.ToLower() == "update" || this.Context.MessageName.ToLower() == "create")
                {
                    bool doRenewalCheck = false;
                    if (entityPreImage.Contains("crmd_membershipstatus"))
                    {
                        int premembershipvalue = entityPreImage.GetAttributeValue<OptionSetValue>("crmd_membershipstatus").Value;
                        int currentStatus = premembershipvalue;

                        //if altus_membershipstatus changed
                        if (this.Context.MessageName.ToLower() == "update")
                        {
                            if (entity.Attributes.Contains("crmd_membershipstatus"))
                            {
                                var newStatus = entity.GetAttributeValue<OptionSetValue>("crmd_membershipstatus").Value;
                                if (newStatus != currentStatus)
                                {
                                    currentStatus = newStatus;
                                    doRenewalCheck = true;
                                }
                            }
                        }
                        else if (this.Context.MessageName.ToLower() == "create")
                        {
                            doRenewalCheck = true;
                        }
                        //TFS909, need to have contact related to this membership
                        if (doRenewalCheck && entityPreImage.Attributes.Contains(parentMemField) && entityPreImage.Attributes.Contains("crmd_contactid"))
                        {
                            Guid theContactid = entityPreImage.GetAttributeValue<EntityReference>("crmd_contactid").Id;
                            if (this.Context.MessageName.ToLower() == "update" && entity.Attributes.Contains("crmd_contactid"))
                            {
                                theContactid = entity.GetAttributeValue<EntityReference>("altus_contactid").Id;
                            }

                            //only for membership status "current" which means payment processed
                            if (currentStatus == 790410000)//current
                            {
                                this.CheckRenewalTermForThisMembership(this.Service, entityPreImage.GetAttributeValue<EntityReference>(parentMemField).Id, theContactid, true);
                            }
                            else if (currentStatus == 790410005)//cancel, including left org, do not renew, remove action on membership
                            {
                                //remove the renewal counterparts
                                CheckRenewalTermForThisMembership(this.Service, entityPreImage.GetAttributeValue<EntityReference>(parentMemField).Id, theContactid, false);
                            }
                        }

                        //this is to track membership status change from "pending" to "current"
                        if (premembershipvalue == 790410004)
                        {
                            int membershipvalue = (entity.Attributes.Contains("crmd_membershipstatus") && entity.Attributes["crmd_membershipstatus"] != null ? ((OptionSetValue)entity.Attributes["crmd_membershipstatus"]).Value : 0);
                            //altus_membershipstatus
                            // 790410004 pending
                            // 790410000 Current
                            //the following code will be run when membership first created with paidDate populated where membershipstatus is pending 
                            Timeline timeLine = new Timeline();
                            if (membershipvalue != 790410000)
                            {
                                if (entity.Contains("crmd_paymentdate"))//when membership created, it always has "altus_paymentdate" field, it could be null
                                {
                                    //tracingService.Trace("Has Payment Date");

                                    int membershipactionvalue = (entityPreImage.Attributes.Contains("crmd_membershipaction") && entityPreImage.Attributes["crmd_membershipaction"] != null ? ((OptionSetValue)entityPreImage.Attributes["crmd_membershipaction"]).Value : 0);
                                    DateTime LastDate = DateTime.MinValue;
                                    DateTime TempLastDate = DateTime.MinValue;
                                    DateTime FirstDayofMonth = DateTime.MinValue;
                                    DateTime paymentDate;

                                    DateTime prepaymentDate = DateTime.MinValue;
                                    DateTime renewaldate = DateTime.MinValue;

                                    //membership action only 2 option new/renewal
                                    //790410000 new
                                    //790410001 renewal
                                    if (membershipactionvalue == 790410000)//new membership
                                    {
                                        //tracingService.Trace("New Mem");

                                        prepaymentDate = (entityPreImage.Attributes.Contains("crmd_paymentdate") && entityPreImage.Attributes["crmd_paymentdate"] != null ? ((DateTime)entityPreImage.Attributes["crmd_paymentdate"]) : DateTime.MinValue);
                                        paymentDate = (entity.Attributes.Contains("crmd_paymentdate") && entity.Attributes["crmd_paymentdate"] != null ? ((DateTime)entity.Attributes["crmd_paymentdate"]) : DateTime.MinValue);

                                        if ((this.Context.MessageName.ToLower() == "update" && paymentDate != prepaymentDate) ||
                                            (this.Context.MessageName.ToLower() == "create" && paymentDate > minCrmDate))
                                        {
                                            //Calculate Start of the month base on Payment Date  
                                            FirstDayofMonth = new DateTime(paymentDate.Year, paymentDate.Month, 1);

                                            Entity membershipTypes = this.Service.Retrieve("crmd_membershiptype", entityPreImage.GetAttributeValue<EntityReference>("crmd_membershiptypeid").Id, new ColumnSet(true));

                                            timeLine = MembershipHelper.GetTimeline(membershipTypes, paymentDate);

                                            MembershipHelper.CheckReferralFromMembership(this.Service, entityPreImage, entity, this.Tracing, paymentDate);
                                        }

                                    }//membershipactionvalue == 790410000
                                    else//renewal membership
                                    {
                                        prepaymentDate = (entityPreImage.Attributes.Contains("crmd_paymentdate") && entityPreImage.Attributes["crmd_paymentdate"] != null ? ((DateTime)entityPreImage.Attributes["crmd_paymentdate"]) : DateTime.MinValue);
                                        paymentDate = (entity.Attributes.Contains("crmd_paymentdate") && entity.Attributes["crmd_paymentdate"] != null ? ((DateTime)entity.Attributes["crmd_paymentdate"]) : DateTime.MinValue);

                                        //processed in Corp portal may only have account, no contact
                                        if (entityPreImage.Attributes.Contains("crmd_contactid"))
                                        {
                                            //Calculate Start of the month base on Payment Date
                                            Entity myContact = this.Service.Retrieve("contact", entityPreImage.GetAttributeValue<EntityReference>("crmd_contactid").Id, new ColumnSet(true));

                                            if (myContact != null)
                                            {
                                                renewaldate = (myContact.Attributes.Contains("crmd_nextrenewaldatemembership") && myContact.Attributes["crmd_nextrenewaldatemembership"] != null ? ((DateTime)myContact.Attributes["crmd_nextrenewaldatemembership"]) : DateTime.MinValue);
                                            }

                                            Entity membershipTypes = this.Service.Retrieve("crmd_membershiptype", entityPreImage.GetAttributeValue<EntityReference>("crmd_membershiptypeid").Id, new ColumnSet(true));
                                            timeLine = MembershipHelper.GetTimeline(membershipTypes, renewaldate.AddDays(1));
                                        }
                                    }

                                    if (entityPreImage.GetAttributeValue<DateTime>("crmd_startdate") < minCrmDate)
                                    {
                                        if (entity.GetAttributeValue<DateTime>("crmd_paymentdate") > minCrmDate && timeLine.StartDate > minCrmDate && timeLine.EndDate > minCrmDate)
                                        {
                                            entity.Attributes["crmd_startdate"] = timeLine.StartDate;
                                            entity.Attributes["adx_renewaldate"] = timeLine.EndDate;

                                            //create is Post-op while update is pre-op
                                            if (this.Context.MessageName.ToLower() == "create")
                                            {
                                                var MemToUpdate = new Entity("crmd_membership");
                                                MemToUpdate.Id = entity.Id;
                                                MemToUpdate.Attributes["crmd_startdate"] = timeLine.StartDate;
                                                MemToUpdate.Attributes["adx_renewaldate"] = timeLine.EndDate;

                                                MemToUpdate.Attributes["crmd_membershipstatus"] = new OptionSetValue(790410000);//current
                                                this.Service.Update(MemToUpdate);
                                            }
                                        }
                                    }

                                    if (entity.GetAttributeValue<DateTime>("crmd_paymentdate") > minCrmDate)
                                    {
                                        MembershipHelper.UpdateContactInfoFromMembership(this.Service, entityPreImage, entity, timeLine.StartDate, timeLine.EndDate, this.Context.MessageName);
                                    }


                                }//entity.Contains("altus_paymentdate")
                            }// if (membershipvalue != 790410000)
                            else//if it is from pending to current
                            {
                                if (entityPreImage.GetAttributeValue<DateTime>("crmd_startdate") < minCrmDate)
                                {
                                    if (entityPreImage.GetAttributeValue<DateTime>("crmd_paymentdate") > minCrmDate)
                                    {
                                        Entity membershipTypes = this.Service.Retrieve("crmd_membershiptype", entityPreImage.GetAttributeValue<EntityReference>("crmd_membershiptypeid").Id, new ColumnSet(true));
                                        var paidDate = MembershipHelper.GetLatestDateTime(entityPreImage, entity, "crmd_paymentdate");

                                        timeLine = MembershipHelper.GetTimeline(membershipTypes, paidDate);

                                        entity.Attributes["crmd_startdate"] = timeLine.StartDate;
                                        entity.Attributes["crmd_renewaldate"] = timeLine.EndDate;

                                        MembershipHelper.UpdateContactInfoFromMembership(this.Service, entityPreImage, entity, timeLine.StartDate, timeLine.EndDate, this.Context.MessageName);

                                        MembershipHelper.CheckReferralFromMembership(this.Service, entityPreImage, entity, this.Tracing, paidDate);
                                    }
                                }
                                else if (entityPreImage.GetAttributeValue<DateTime>("crmd_startdate") > minCrmDate && entityPreImage.GetAttributeValue<DateTime>("crmd_renewaldate") > minCrmDate)
                                {
                                    //TFS 1175, verify renewal term
                                    if (entityPreImage.Attributes.Contains(parentMemField) && entityPreImage.Attributes.Contains("crmd_contactid"))
                                    {
                                        Guid theContactid = entityPreImage.GetAttributeValue<EntityReference>("crmd_contactid").Id;
                                        CheckRenewalTermForThisMembership(this.Service, entityPreImage.GetAttributeValue<EntityReference>(parentMemField).Id, theContactid, true);
                                    }

                                    MembershipHelper.UpdateContactInfoFromMembership(this.Service, entityPreImage, entity, entityPreImage.GetAttributeValue<DateTime>("crmd_startdate"), entityPreImage.GetAttributeValue<DateTime>("crmd_renewaldate"), this.Context.MessageName);
                                }
                            }
                        }//pre membershipvalue == 790410004  pending
                        //790410006 renewed
                        else if (premembershipvalue == 790410006)
                        {
                            int membershipvalue = (entity.Attributes.Contains("crmd_membershipstatus") && entity.Attributes["crmd_membershipstatus"] != null ? ((OptionSetValue)entity.Attributes["crmd_membershipstatus"]).Value : 0);
                            //altus_membershipstatus
                            // 790410004 pending
                            // 790410000 Current

                            //from Renewed to Current

                            if (membershipvalue == 790410000)
                            {
                                MembershipHelper.UpdateContactInfoFromMembership(this.Service, entityPreImage, entity, entityPreImage.GetAttributeValue<DateTime>("crmd_startdate"), entityPreImage.GetAttributeValue<DateTime>("crmd_renewaldate"), this.Context.MessageName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void CheckRenewalTermForThisMembership(IOrganizationService service, Guid parentMemId, Guid contactId, bool createOrRemove)
        {
            //try
            //{
            //    Entity parentMemEntity = service.Retrieve("adx_membership", parentMemId, new ColumnSet(true));
            //    //790410000  current period
            //    if (parentMemEntity != null && parentMemEntity.GetAttributeValue<OptionSetValue>("altus_parentmembershipperiod") != null && parentMemEntity.GetAttributeValue<OptionSetValue>("altus_parentmembershipperiod").Value == 790410000 && parentMemEntity.GetAttributeValue<bool>("altus_iscompanymembership") && parentMemEntity.Attributes.Contains("adx_accountid"))
            //    {
            //        //find the renewal term
            //        var parentMemberships = Membership.getParentMembershipsByAccount(service, parentMemEntity.GetAttributeValue<EntityReference>("adx_accountid").Id);

            //        if (parentMemberships.Entities.Count > 0)
            //        {
            //            Guid renewalParentMemId = Guid.Empty;
            //            foreach (var item in parentMemberships.Entities)
            //            {
            //                //renewal period 790410001
            //                //if membership status is Renewed , then ignore as it was already renewed
            //                if (item.Attributes.Contains("altus_parentmembershipperiod") && item.GetAttributeValue<OptionSetValue>("altus_parentmembershipperiod").Value == 790410001 && item.Attributes.Contains("altus_membershipstatus") &&
            //                    item.GetAttributeValue<OptionSetValue>("altus_membershipstatus").Value != 790410006)
            //                {
            //                    renewalParentMemId = item.Id;
            //                }
            //            }

            //            if (renewalParentMemId != Guid.Empty)
            //            {
            //                Util.writeDebugDataJSON("Membership_PreUpdate_CheckRenewal", renewalParentMemId);
            //                var existingRenewalMemberships = Membership.getMembershipByParentMembershipAndContact(service, renewalParentMemId, contactId);
            //                //if contact do not have a renewal membership, create one
            //                if (existingRenewalMemberships.Entities.Count == 0 && createOrRemove)
            //                {
            //                    Util.CreateMembershipBasedOnExistingMembership(service, contactId, renewalParentMemId, true, true, false);
            //                }
            //                else if (!createOrRemove && existingRenewalMemberships.Entities.Count == 1)
            //                {
            //                    //if contact do have a renewal membership, remove this one
            //                    var uMembership = new Entity("adx_membership");
            //                    uMembership.Id = existingRenewalMemberships.Entities[0].Id;

            //                    uMembership["altus_membershipstatus"] = new OptionSetValue(790410005);//Canceled

            //                    uMembership["adx_comments"] = "This pending period membership canceled due to current period membership had been canceled";
            //                    service.Update(uMembership);

            //                    Util.writeDebugDataJSON("Real_time_renewal_remove", existingRenewalMemberships.Entities[0]);
            //                }
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Util.writeDebugDataJSON("MemberShipPreUpdateCreate_ERROR1_" + ex.Message, ex);
            //}
        }
    }
}